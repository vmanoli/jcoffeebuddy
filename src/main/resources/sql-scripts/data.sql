INSERT INTO item_group (id, name, description) VALUES (1, 'Καφέδες', 'Όλοι οι καφέδες');
INSERT INTO item_group (id, name, description) VALUES (2, 'Σφολιάτες', 'Όλες οι σφολιάτες πίτες τυρόπιτες, κλπ');

INSERT INTO item (id, item_group_id, name, image, extras) VALUES (1, 1, 'Freddo espresso', 'coffee.jpg', 'σκέτος');
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (2, 1, 'Freddo espresso', 'coffee.jpg', 'μέτριος');
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (3, 1, 'Freddo espresso', 'coffee.jpg', 'γλυκός');
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (4, 2, 'Τυρόπιτα', 'pie1.png', NULL);
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (5, 2, 'Κασερόπιτα', 'pie1.png', NULL);
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (6, 2, 'Ζαμπονοτυρόπιτα', 'pie1.png', NULL);
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (7, 2, 'Κρουασάν', 'croissant2.jpg', 'με βούτυρο');
INSERT INTO item (id, item_group_id, name, image, extras) VALUES (8, 2, 'Κρουασάν', 'croissant1.jpg', 'με σοκολάτα');

INSERT INTO app_role (id, role_name, description) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');


INSERT INTO user_profile (id, description, email, address, city, phone) VALUES (1, 'I am a cheerful happy customer', 'vmanoli@upnet.gr', NULL, NULL, NULL);
INSERT INTO user_profile (id, description, email, address, city, phone) VALUES (2, 'I am a cheerful happy admin', 'v@upnet.gr', NULL, NULL, NULL);
INSERT INTO user_profile (id, description, email, address, city, phone) VALUES (3, 'I am a cheerful happy girl', 'm@upnet.gr', NULL, NULL, NULL);


-- USER
-- non-encrypted password: jwtpass
INSERT INTO app_user (id, first_name, last_name, password, username, profile_id) VALUES (1, 'John', 'Doe', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'john.doe', 1);
INSERT INTO app_user (id, first_name, last_name, password, username, profile_id) VALUES (2, 'Admin', 'Admin', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'admin.admin', 2);
INSERT INTO app_user (id, first_name, last_name, password, username, profile_id) VALUES (3, 'Mary', 'Doe', '821f498d827d4edad2ed0960408a98edceb661d9f34287ceda2962417881231a', 'mary.doe', 3);


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);


INSERT INTO favorite_item(profile_id, item_id) VALUES(1,3);
INSERT INTO favorite_item(profile_id, item_id) VALUES(1,4);
INSERT INTO favorite_item(profile_id, item_id) VALUES(2,1);
INSERT INTO favorite_item(profile_id, item_id) VALUES(2,5);
INSERT INTO favorite_item(profile_id, item_id) VALUES(2,8);

-- Populate random city table

INSERT INTO random_city(id, name) VALUES (1, 'Bamako');
INSERT INTO random_city(id, name) VALUES (2, 'Nonkon');
INSERT INTO random_city(id, name) VALUES (3, 'Houston');
INSERT INTO random_city(id, name) VALUES (4, 'Toronto');
INSERT INTO random_city(id, name) VALUES (5, 'New York City');
INSERT INTO random_city(id, name) VALUES (6, 'Mopti');
INSERT INTO random_city(id, name) VALUES (7, 'Koulikoro');
INSERT INTO random_city(id, name) VALUES (8, 'Moscow');

INSERT INTO `order_placement`(id, name, created_date, closed_date, created_by, closed) VALUES
(1, 'Order open', NOW(), NULL, 1, 0),
(2, 'Order closed', NOW(), NOW(), 1, 1);

INSERT INTO order_item(order_id, item_id, user_id) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 4, 2),
(2, 2, 1),
(2, 3, 2),
(2, 5, 2);

INSERT INTO `room`(id, name, created_date, closed_date, created_by, closed) VALUES
(1, 'My office', NOW(), NULL, 1, 0),
(2, 'Your office', NOW(), NULL, 1, 0);

INSERT INTO `room_order` (room_id, order_id) VALUES
(1, 1) , (1, 2);

INSERT INTO `room_user` (room_id, user_id) VALUES
(1, 1) , (1, 3), (2, 2) , (2, 3);