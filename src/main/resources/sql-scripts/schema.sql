CREATE TABLE item_group (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  description varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE item (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  image varchar(255) DEFAULT NULL,
  extras varchar(255) DEFAULT NULL,
  item_group_id bigint(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE random_city (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE app_role (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  role_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE user_profile (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  description varchar(255) NOT NULL,
  email varchar(255) NULL,
  address varchar(255) NULL,
  city varchar(255) NULL,
  phone varchar(255) NULL,
  PRIMARY KEY (id)
);

CREATE TABLE app_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  profile_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_appuser_userprofile FOREIGN KEY (profile_id) REFERENCES user_profile (id)
);


CREATE TABLE user_role (
  user_id bigint(20) NOT NULL,
  role_id bigint(20) NOT NULL,
  CONSTRAINT FK859n2jvi8ivhui0rl0esws6o FOREIGN KEY (user_id) REFERENCES app_user (id),
  CONSTRAINT FKa68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES app_role (id)
);


CREATE TABLE favorite_item (
  profile_id bigint(20) NOT NULL,
  item_id bigint(20) NOT NULL,
  CONSTRAINT fk_favoriteitem_userprofile FOREIGN KEY (profile_id) REFERENCES user_profile (id),
  CONSTRAINT fk_favoriteitem_item FOREIGN KEY (item_id) REFERENCES item (id)
);

CREATE TABLE `order_placement` (
  id bigint(20) NOT NULL,
  name varchar(255) DEFAULT NULL,
  created_date DATETIME NOT NULL,
  closed_date DATETIME DEFAULT NULL,
  created_by bigint(20) DEFAULT NULL,
  closed boolean DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE order_item (
  order_id bigint(20) NOT NULL,
  item_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  CONSTRAINT fk_orderitem_order FOREIGN KEY (order_id) REFERENCES order_placement (id),
  CONSTRAINT fk_orderitem_item FOREIGN KEY (item_id) REFERENCES item (id),
  CONSTRAINT fk_orderitem_appuser FOREIGN KEY (user_id) REFERENCES app_user (id)
);

CREATE TABLE `room` (
  id bigint(20) NOT NULL,
  name varchar(255) DEFAULT NULL,
  created_date DATETIME NOT NULL,
  closed_date DATETIME DEFAULT NULL,
  created_by bigint(20) DEFAULT NULL,
  closed boolean DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE room_order (
  room_id bigint(20) NOT NULL,
  order_id bigint(20) NOT NULL,
  CONSTRAINT fk_roomorder_room FOREIGN KEY (room_id) REFERENCES room (id),
  CONSTRAINT fk_roomorder_order FOREIGN KEY (order_id) REFERENCES order_placement (id)
);

CREATE TABLE room_user (
  room_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  CONSTRAINT fk_roomuser_room FOREIGN KEY (room_id) REFERENCES room (id),
  CONSTRAINT fk_roomuser_user FOREIGN KEY (user_id) REFERENCES app_user (id)
);











