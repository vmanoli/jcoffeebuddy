package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.Profile;
import org.springframework.data.repository.CrudRepository;

/**
 * vicky on 10/05/17.
 */
public interface ProfileRepository extends CrudRepository<Profile, Long> {
}
