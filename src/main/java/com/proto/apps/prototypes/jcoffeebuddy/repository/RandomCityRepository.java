package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.RandomCity;
import org.springframework.data.repository.CrudRepository;

/**
 * vicky on 10/05/17.
 */
public interface RandomCityRepository extends CrudRepository<RandomCity, Long> {
}
