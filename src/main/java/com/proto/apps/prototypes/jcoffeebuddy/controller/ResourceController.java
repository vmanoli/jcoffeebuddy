package com.proto.apps.prototypes.jcoffeebuddy.controller;

import com.proto.apps.prototypes.jcoffeebuddy.domain.RandomCity;
import com.proto.apps.prototypes.jcoffeebuddy.domain.User;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Item;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Order;
import com.proto.apps.prototypes.jcoffeebuddy.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * vicky on 06/05/17.
 */
@RestController
@RequestMapping("/api")
public class ResourceController {
    @Autowired
    private GenericService userService;

    @RequestMapping(value ="/cities")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public List<RandomCity> getCities(){
        return userService.findAllRandomCities();
    }

    @RequestMapping(value ="/items", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public List<Item> getItems(){
        return userService.findAllItems();
    }

    @RequestMapping(value ="/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public List<User> getUsers(){
        return userService.findAllUsers();
    }

    @RequestMapping(value ="/users/{userId}/favorites", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public List<Item> getFavorites(@PathVariable Long userId){
        return userService.findFavoritesForUser(userId);
    }

    @RequestMapping(value ="/orders", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public List<Order> getOrders(){
        return userService.findAllOrders();
    }

}
