package com.proto.apps.prototypes.jcoffeebuddy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JcoffeeBuddyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JcoffeeBuddyApplication.class, args);
	}
}
