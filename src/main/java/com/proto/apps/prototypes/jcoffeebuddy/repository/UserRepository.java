package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * vicky on 06/05/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    User findById(Long id);
}
