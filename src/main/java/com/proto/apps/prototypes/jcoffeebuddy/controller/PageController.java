package com.proto.apps.prototypes.jcoffeebuddy.controller;

import com.proto.apps.prototypes.jcoffeebuddy.domain.RandomCity;
import com.proto.apps.prototypes.jcoffeebuddy.domain.User;
import com.proto.apps.prototypes.jcoffeebuddy.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * vicky on 06/05/17.
 */
@RestController
@RequestMapping("/")
public class PageController {
    @Autowired
    private GenericService userService;

    @RequestMapping("/")
    public String index(){
        return "Welcome to Coffee Buddy!";
    }

}
