package com.proto.apps.prototypes.jcoffeebuddy.service;

import com.proto.apps.prototypes.jcoffeebuddy.domain.RandomCity;
import com.proto.apps.prototypes.jcoffeebuddy.domain.User;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Item;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Order;

import java.util.List;

/**
 * vicky on 06/05/17.
 */
public interface GenericService {
    User findByUsername(String username);

    List<User> findAllUsers();

    List<RandomCity> findAllRandomCities();

    List<Item> findAllItems();

    List<Item> findFavoritesForUser(Long id);

    List<Order> findAllOrders();

}
