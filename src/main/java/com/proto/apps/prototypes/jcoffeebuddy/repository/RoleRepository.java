package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * vicky on 06/05/17.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
}
