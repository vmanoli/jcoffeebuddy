package com.proto.apps.prototypes.jcoffeebuddy.service.impl;

import com.proto.apps.prototypes.jcoffeebuddy.domain.RandomCity;
import com.proto.apps.prototypes.jcoffeebuddy.domain.User;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Item;
import com.proto.apps.prototypes.jcoffeebuddy.domain.Order;
import com.proto.apps.prototypes.jcoffeebuddy.repository.RandomCityRepository;
import com.proto.apps.prototypes.jcoffeebuddy.repository.UserRepository;
import com.proto.apps.prototypes.jcoffeebuddy.repository.ItemRepository;
import com.proto.apps.prototypes.jcoffeebuddy.repository.OrderRepository;
import com.proto.apps.prototypes.jcoffeebuddy.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.List;

/**
 * vicky on 07/05/17.
 */
@Service
public class GenericServiceImpl implements GenericService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RandomCityRepository randomCityRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>)userRepository.findAll();
    }

    @Override
    public List<RandomCity> findAllRandomCities() {
        return (List<RandomCity>)randomCityRepository.findAll();
    }

    public List<Item> findAllItems() {
        return (List<Item>)itemRepository.findAll();
    }

    public List<Item> findFavoritesForUser(Long id) throws NoSuchElementException {
        User u = userRepository.findById(id);
        return (List<Item>)u.getProfile().getFavoriteItems();
    }

    @Override
    public List<Order> findAllOrders() {
        return (List<Order>)orderRepository.findAll();
    }
}
