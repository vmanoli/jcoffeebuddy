package com.proto.apps.prototypes.jcoffeebuddy.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.Date;

/**
 * vicky on 06/05/17.
 */
@Entity
@Table(name = "room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "closed_date")
    private Date closedDate;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "closed")
    private Boolean closed;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "room_order", joinColumns
            = @JoinColumn(name = "room_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "order_id",
                    referencedColumnName = "id"))
    private List<Order> orders;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "room_user", joinColumns
            = @JoinColumn(name = "room_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id",
                    referencedColumnName = "id"))
    private Set<User> users;

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        if (closed == true)
            this.closedDate = new Date();
        else
            this.closedDate = null;

        this.closed = closed;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getClosedDate() {
        return createdDate;
    }

    public void setClosedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long id) {
        this.createdBy = id;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setItems(List<Order> orders) {
        this.orders = orders;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    // public Boolean addUser(User user) {
    //     return this.users.add(user);
    // }

    // public Boolean removeUser(){}

}

