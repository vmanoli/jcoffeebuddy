package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.Item;
import org.springframework.data.repository.CrudRepository;

/**
 * vicky on 10/05/17.
 */
public interface ItemRepository extends CrudRepository<Item, Long> {
}
