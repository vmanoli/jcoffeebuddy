package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * vicky on 06/05/17.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
    Order findById(Long id);
}
