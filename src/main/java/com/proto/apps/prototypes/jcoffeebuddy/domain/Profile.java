package com.proto.apps.prototypes.jcoffeebuddy.domain;

import javax.persistence.*;
import java.util.List;

/**
 * vicky on 10/05/17.
 */
@Entity
@Table(name = "user_profile")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "phone")
    private String phone;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "favorite_item", joinColumns
            = @JoinColumn(name = "profile_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "item_id",
                    referencedColumnName = "id"))
    private List<Item> favoriteItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Item> getFavoriteItems() {
        return favoriteItems;
    }

    public void setFavoriteItems(List<Item> favoriteItems) {
        this.favoriteItems = favoriteItems;
    }
}
