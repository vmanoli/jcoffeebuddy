package com.proto.apps.prototypes.jcoffeebuddy.repository;

import com.proto.apps.prototypes.jcoffeebuddy.domain.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * vicky on 02/12/17.
 */
public interface RoomRepository extends CrudRepository<Room, Long> {
    Room findById(Long id);
}
